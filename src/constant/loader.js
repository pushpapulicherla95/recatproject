import React from "react";
import '../../public/css/style.css';

class Loader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        console.log("props data in loader", this.props);
        return (
            <div>
                {this.props.data &&
                    // <div className="cssload-loader  cssload-triangles"></div>
                    <div className='art-loader'>
                        <div className='loader'>
                            <div className='circle'></div>
                            <div className='circle'></div>
                            <div className='circle'></div>
                            <div className='circle'></div>
                            <div className='circle'></div>
                        </div>
                    </div>

                }
            </div>


        );
    }
}
export default Loader;
