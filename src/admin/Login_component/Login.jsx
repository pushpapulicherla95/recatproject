import React from 'react';
import '../../../public/css/style.css';
import '../../../public/css/responsive.css';
import '../../../public/css/reset.css';
import '../../../public/css/font-awesome.css';
import { HashLoader } from 'react-spinners';
import { API_BASE_URL } from '../../constant/baseurl.js';
import Loader from '../../constant/loader.js';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';

import Notifications, { notify } from 'react-notify-toast';
import { NavLink } from 'react-router-dom';
import axios from 'axios';

class Login extends React.Component {
	constructor(props) {
		super(props);
		console.log("constroctor>>");
		this.state = {
			errorResponse: '',
			emailId: '',
			password: '',
			errors: {},
			passwordValid: false,
			emailValid: false,
			disabled: false,
			formValid: false,
			loading: false
		}
		this.responseGoogle = this.responseGoogle.bind(this);
		this.responseFacebook = this.responseFacebook.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleClick = this.handleClick.bind(this);
		this.responseFacebook = this.responseFacebook.bind(this);

	}
	componentDidMount() {
		console.log("login component did mount")
	}
	handleChange(event) {
		event.preventDefault();
		this.setState({ [event.target.name]: event.target.value });
		let myColor = { background: 'red', text: "#FFFFFF" };

	}

	validateField(value) {
		const errors = {};
		let emailValid;
		if (value) {
			emailValid = value.emailId.match(/^([a-z0-9._%+-]+)@([a-z0-9.-])+(\.[a-z]{2,3})$/i);
			if (typeof value.emailId !== 'string' || value.emailId.trim().length === 0 || emailValid === null) {
				errors.emailId = 'Please enter valid Email Address';
			}
			if (typeof value.password !== 'string' || value.password.trim().length === 0) {
				errors.password = 'Please enter valid Password.';
			}
		}
		return errors;
	}

	isEmpty(obj) {

		if (obj == null) return true;
		if (obj.length > 0) return false;
		if (obj.length === 0) return true;
		if (typeof obj !== "object") return true;
		for (var x in obj) {
			return false;
		}

		return true;

	}

	handleClick(event) {
		event.preventDefault();
		var payload = {
			"emailId": this.state.emailId,
			"password": this.state.password
		}

		let errors = this.validateField(payload);
		let test = {};
		if (this.isEmpty(errors)) {
			this.setState({ errors: '' });
			this.serviceHandler(payload);
		}
		else {
			this.setState({ errors });
			setTimeout(() => this.setState({ errors: '' }), 50000);
		}
	}
	serviceHandler(payload) {
		const props = this.props;
		const apiBaseUrl = API_BASE_URL + "artcoin/api/login";
		var self = this;
		this.setState({ loading: true });
		axios.post(apiBaseUrl, payload)
			.then(res => {
				console.log("test login successs datata>>>>", res);
				if (res.status == 200) {
					sessionStorage.setItem('userData', JSON.stringify(res.data.loginInfo));
					if (res.data.loginInfo.roleId == 1) {
						props.history.push('/admindashboard');
						notify.show(res.data.message,"success");

					} else if (res.data.loginInfo.roleId == 2) {
						props.history.push('/userdashboard');
						notify.show(res.data.message, "success");
					}



				}
				else {
					this.setState({ loading: false });
					notify.show(res.data.message, "error");

					self.setState({ errorResponse: res.data.message });
					setTimeout(() => self.setState({ errorResponse: '' }), 5000);

				}
			})
			.catch(function (error) {
				this.setState({ loading: false });
				self.setState({ errorResponse: error.message });
				setTimeout(() => self.setState({ errorResponse: '' }), 5000);

			});
	}

	responseGoogle(response) {
		
		console.log("google response>>", response);
		let setval = this;
		console.log("setval",this);
		const tokenUrl = API_BASE_URL + "artcoin/api/user/socialNetwork/register";
		let details ={
			"userName":response.profileObj.name,
			"emailId":response.profileObj.email
		
		}
		console.log("details>>>",details);
		this.setState({ loading: true });
		axios.post(tokenUrl, details)
		.then(res => {
			console.log("google successs datata>>>>", res);
			if (res.status == 200) {
				this.setState({ loading: false });
				sessionStorage.setItem('userData', JSON.stringify(res.data.loginInfo));
				if (res.data.loginInfo.roleId == 1) {
					setval.props.history.push('/admindashboard');
					notify.show(res.data.message, "success");

				} else if (res.data.loginInfo.roleId == 2) {
					setval.props.history.push('/userdashboard');
					notify.show(res.data.message, "success");
				}
			
			}
			else {
				this.setState({ loading: false });
				console.log("gmail error",response);
			}
		})
		.catch(function (error) {
			this.setState({ loading: false });
			console.log("gmail catch",error);

		});

	}

	responseFacebook(response) {
		console.log("this.props", this.props);
		console.log("facebook response", response); 
		let setval = this;
		this.setState({ loading: true });
		if (response.userID) {
			const tokenUrl = API_BASE_URL + "artcoin/api/user/socialNetwork/register";
			let details ={
				"userName":response.name,
				"emailId":response.email
			
			}
			console.log("details>>>",details);
			axios.post(tokenUrl, details)
			.then(res => {
				console.log("facebook successs datafgfgfgta>>>>", res);
				if (res.status == 200) {
					this.setState({ loading: false });
					sessionStorage.setItem('userData', JSON.stringify(res.data.loginInfo));
					if (res.data.loginInfo.roleId == 1) {
						setval.props.history.push('/admindashboard');
						notify.show(res.data.message, "success");

					} else if (res.data.loginInfo.roleId == 2) {
						console.log("sdsdsdsd");
						console.log();
						setval.props.history.push('/userdashboard');
						notify.show(res.data.message, "success");
					}
				
				}
				else {
					if (res.data.message == 'Session Expired') {
                        setval.props.history.push('/login');
                    }
					this.setState({ loading: false });
					console.log("face errrrr",response);
				}
			})
			.catch(function (error) {
				this.setState({ loading: false });
				console.log(" face catch",error);
	
			});
		}
	}


	render() {
		console.log("second console");
		return (
			<div className="login-fullwrap">
				<Notifications />
				<div className="login-section">
					<div className="login-body">
						<form>

							<div className="form-logo txt-center">
								<img src="public/image/logo.png" alt="logo" />
							</div>
							<div className="form-heading txt-center">

								<div className="form-title">Login</div>
								<div className="social-line">
									<ul>
										<li>
											<a href="javascript:void(0)">
												{/* <i className="fa fa-facebook" aria-hidden="true"></i> */}
												
											<FacebookLogin
												cssClass="btn-social btn-facebook btn-flat fa fa-facebook"
												appId="472571439807443"
												autoLoad={false}
												textButton=""
												fields="name,email,picture"
												callback={this.responseFacebook}
											/>
											</a>

										</li>
										{/* <li>
											<a href="javascript:void(0)">
												<i className="fa fa-twitter" aria-hidden="true"></i>
											</a>
										</li> */}
										<li>
											<a href="">
												{/* <i className="fa fa-google-plus" aria-hidden="true"></i> */}
												<GoogleLogin
													className="btn-social btn-google btn-flat fa fa-google fa fa-google-plus"

													clientId="97290228343-jqojgvqe48fp9ho8tnvoce0mp9455p7i.apps.googleusercontent.com"
													buttonText=""
													autoLoad={false}
													onSuccess={this.responseGoogle}
													onFailure={this.responseGoogle}
												/>

											</a>


											{/* <GoogleLogin
                                                className="btn-social btn-google btn-flat fa fa-google"
                                                clientId="707462947564-02h02mrsp4b153o0tj7fcvluikmc0g3b"
                                                buttonText="Login With Google"
                                                onSuccess={this.responseGoogle}
                                                onFailure={this.responseGoogle}
											/> */}

										</li>
									</ul>
								</div>
								<div className="login-form">
									<Notifications />
									<div className="form-group">
										<input type="text" placeholder="Email" name='emailId' onChange={this.handleChange} required />
										{this.state.errors.emailId &&
											<div className="form__field" style={{ color: 'red' }}>{this.state.errors.emailId}</div>
										}
									</div>
									<div className="form-group">
										<input type="password" placeholder="Password" name='password' onChange={this.handleChange} required />
										{this.state.errors.password &&
											<div className="form__field" style={{ color: 'red' }}>{this.state.errors.password}</div>
										}
									</div>
									<div className="form-group">
										<div className="remeber-me">
											<input type="checkbox" /> Remember Me
                                		</div>
										<div className="forgot-pwd">
											<NavLink to={'/Forgotpassword'}>Forgot Password?</NavLink>
										</div>
									</div>
								
									<div className="form-group" style={{ marginTop: '35px' }}>
										<button type="button" className="btn btn-login" type='submit' onClick={this.handleClick}>Login</button>
									</div>

									<div className="form-group">
										<p className="account-reg">Dont’t have an Account? 
                                    <NavLink to={'/register'}> Register Now</NavLink>
										</p>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<Loader data={this.state.loading} />



			</div>
		)
	}
}

export default Login;