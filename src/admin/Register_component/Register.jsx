import React from 'react';
import { NavLink } from 'react-router-dom';
import validator from 'validator';
import axios from 'axios';
import ReactFlagsSelect from 'react-flags-select';

import '../../../public/css/style.css';
import '../../../public/css/responsive.css';
import '../../../public/css/reset.css';
import '../../../public/css/font-awesome.css';
import './../../../public/flags.png';

import { API_BASE_URL } from '../../constant/baseurl.js';
import Notifications, { notify } from 'react-notify-toast';
import queryString from 'query-string';
// import ReactTelInput from 'react-telephone-input';
import Loader from '../../constant/loader.js';
// import 'react-telephone-input/lib/withStyles';
// import 'react-telephone-input/css/default.css';
import IntlTelInput from 'react-intl-tel-input';
import 'react-intl-tel-input/dist/libphonenumber.js';
import 'react-intl-tel-input/dist/main.css';
class Register extends React.Component {

    constructor(props) {
        super(props);
        const parsed = queryString.parse(props.location.search);
        console.log("prrrrrrrr", parsed);
        this.state = {
            userName: '',
            mobileNo: '',
            emailId: '',
            password: '',
            isChecked: false,
            confirmPassword: '',
            etherWalletPassword: '',
            errorResponse: '',
            referenceId: parsed.referenceId,
            payload: {},
            errors: {},
            loading: false,
            check: false,
            name: '',
            confMailid: parsed.emailId,
            phonevalid: false
        }

        this.handleClick = this.handleClick.bind(this);
        this.isEmpty = this.isEmpty.bind(this);
        this.serviceHandler = this.serviceHandler.bind(this);
        this.termsConditions = this.termsConditions.bind(this);
        this.pageValidation = this.pageValidation.bind(this);
        this.submitValidations = this.submitValidations.bind(this);
        this.handler = this.handler.bind(this);

        // this.etherWalletPassword = this.etherWalletPassword.bind(this);

    }

    handler(status, value, countryData, number, id) {
        console.log("phone datatya", status, value, countryData, number, id);
        this.setState({
            mobileNo: number,
            phonevalid: status
        });
    }
    pageValidation(event) {
        const errors = {};
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
        this.setState({ errors });
    }
    submitValidations(value) {
        const errors = {};
        console.log("value", value);
        if (value.userName) {
            console.log("");
            if (!validator.isLength(value.userName, 6, 20)) {
                console.log("aaaa");
                errors.userName = 'Username should contain 6-20 characters';
                this.setState({ check: true });
            }
        }


        if (!this.state.phonevalid) {
            errors.mobileNo = 'Please enter valid Mobile Number.';

        }

        if (value.emailId) {
            console.log("value.emailId",value,validator.isEmail(value.emailId));
            console.log("validator.isEmail(value.emailId)",validator.isEmail('foo@bar.com'));
            if (!validator.isEmail(value.emailId)) {
                errors.emailId = 'Please enter valid Email Address';
            }
         

        }

        if (value.password) {
            let re = {
                'capital': /[A-Z]/,
                'digit': /[0-9]/,
                'special': /[*@!#%&()^~{}_]+/
             
            };
            if (!re.capital.test(value.password) || !re.digit.test(value.password) || !re.special.test(value.password)) {
                errors.password = 'Must contain atleast one Uppercase,one Number, one Special Character';
            }
            if (!validator.isLength(value.password, 6, 40)) {
                errors.password = 'Password should be 6-40 Characters';
            }
        }
        if (value.confirmPassword) {
            if (!validator.equals(value.confirmPassword, this.state.password)) {
                errors.confirmPassword = 'Password does not Match';
            }
        }

        if (value.etherWalletPassword) {
            if (!validator.isLength(value.etherWalletPassword, 6, 40)) {
                errors.etherWalletPassword = 'Wallet Password should be 6-40 Characters';
            }
            if (validator.isEmpty(value.etherWalletPassword)) {
                errors.etherWalletPassword = 'Please enter Wallet Password';
            }

        }

        if (value.confirmEtherWalletPassword) {
            if (!validator.equals(value.confirmEtherWalletPassword, this.state.etherWalletPassword)) {
                errors.confirmEtherWalletPassword = 'Password does not Match';
            }
        }

        if (!this.state.isChecked) {
            errors.isChecked = 'Please accept the terms and conditions';
        }





        this.setState({ errors });
        setTimeout(() => this.setState({ errors: '' }), 500000);

        return errors;
    }

    termsConditions(event) {
        this.setState({
            isChecked: !this.state.isChecked,
        });

    }



    isEmpty(obj) {

        if (obj == null) return true;
        if (obj.length > 0) return false;
        if (obj.length === 0) return true;
        if (typeof obj !== "object") return true;
        for (var x in obj) {
            return false;
        }

        return true;

    }

    handleClick(event) {
        event.preventDefault();
        console.log("aasasa", this.state);
        if (this.state.referenceId != undefined) {
            this.state.payload = {
                "userName": this.state.userName,
                "mobileNo": this.state.mobileNo,
                "emailId": this.state.emailId,
                "password": this.state.password,
                "confirmPassword": this.state.confirmPassword,
                "etherWalletPassword": this.state.etherWalletPassword,
                "confirmEtherWalletPassword": this.state.confirmEtherWalletPassword,
                'referenceId': this.state.referenceId

            }

        } else {
            this.state.payload = {
                "userName": this.state.userName,
                "mobileNo": this.state.mobileNo,
                "emailId": this.state.emailId,
                "password": this.state.password,
                "confirmPassword": this.state.confirmPassword,
                "etherWalletPassword": this.state.etherWalletPassword,
                "confirmEtherWalletPassword": this.state.confirmEtherWalletPassword

            }

        }
        console.log("state valuess", this.state);
        console.log("payload>>>?????", this.state.payload);

        let errorResponse = this.submitValidations(this.state.payload);
        console.log("abc", errorResponse);
        // if (errorResponse == '') {
        //     console.log("abc>>>>>>>>>>>>>>>>>>>>>");
        //     this.setState({ errors: '' });
        //     this.serviceHandler(this.state.payload);
        // }

        if (this.isEmpty(errorResponse)) {
            console.log("abc");
            this.setState({ errors: '' });
            this.serviceHandler(this.state.payload);
        }

    }

    serviceHandler(payload) {
        const props = this.props;
        console.log("props>>>", props);
        const apiBaseUrl = API_BASE_URL + "artcoin/api/user/register";
        var self = this;
        console.log("testing submit data>>>>", apiBaseUrl);
        this.setState({ loading: true });
        axios.post(apiBaseUrl, payload)
            .then(response => {
                console.log("test api", response);

                if (response.status == 200) {
                    console.log("response.data.message", response.data.message);
                    props.history.push('/login');
                    notify.show(response.data.message, "success");

                }
                else {
                    if (response.data.message == 'Session Expired') {
                        props.history.push('/login');
                    }
                    notify.show(response.data.message, "error");
                    this.setState({ loading: false });

                    self.setState({ errorResponse: response.data.message });
                    setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                }
            })
            .catch(function (error) {
                console.log("catche", error);
                self.setState({ errorResponse: error.message });
                setTimeout(() => self.setState({ errorResponse: '' }), 5000);

            });
    }
    render() {
        return (
            <div className="login-fullwrap">
                <div className="login-section register-section">
                    <div className="login-body">
                        <form>
                            <div className="form-logo txt-center">
                                <img src="public/image/logo.png" alt="logo" />
                            </div>
                            <div className="form-heading txt-center">

                                <div className="form-title">Register</div>
                                <p className="login-reg">Already have an account?
                                <NavLink to={'/login'}> Login</NavLink>
                                </p>
                                <div className="login-form">
                                    <Notifications />
                                    {
                                        this.state.referenceId && <div className="form-group"> 
                                        <input type="text"  name='referenceId' value={this.state.referenceId} required />
                                       
                                    </div>
                                    }
                                    
                                    <div className="form-group">
                                        <input type="text" placeholder="Name" name='userName' value={this.state.userName} onChange={this.pageValidation} required />
                                        {this.state.errors.userName &&
                                            <div className="form__field" style={{ color: 'red' }}>{this.state.errors.userName}</div>
                                        }
                                    </div>
                                    <div className="form-group">

                                        <IntlTelInput
                                            onPhoneNumberChange={this.handler}
                                            onPhoneNumberBlur={this.handler}
                                            css={['intl-tel-input', 'form-control']}
                                            utilsScript={'libphonenumber.js'}
                                        />
                                        {this.state.errors.mobileNo &&
                                            <div className="form__field" style={{ color: 'red' }}>{this.state.errors.mobileNo}</div>
                                        }
                                    </div>


                                    <div className="form-group">
                                        <input type="text" placeholder="Email" name='emailId' onChange={this.pageValidation} required />
                                        {this.state.errors.emailId &&
                                            <div className="form__field" style={{ color: 'red' }}>{this.state.errors.emailId}</div>
                                        }
                                    </div>
                                    <div className="form-group">
                                        <input type="password" name='password' placeholder="Password" onChange={this.pageValidation} required />
                                        {this.state.errors.password &&
                                            <div className="form__field" style={{ color: 'red' }}>{this.state.errors.password}</div>
                                        }
                                    </div>
                                    <div className="form-group">
                                        <input type="password" name='confirmPassword' placeholder="Confirm Password" onChange={this.pageValidation} required />
                                        {
                                            this.state.errors.confirmPassword &&
                                            <div className="form__field" style={{ color: 'red' }}>{this.state.errors.confirmPassword}</div>
                                        }
                                    </div>
                                    <div className="form-group">
                                        <input type="password" name='etherWalletPassword' placeholder="Wallet Password" onChange={this.pageValidation} required />
                                        {
                                            this.state.errors.etherWalletPassword &&
                                            <div className="form__field" style={{ color: 'red' }}>{this.state.errors.etherWalletPassword}</div>
                                        }
                                    </div>
                                    <div className="form-group"> 
                                        <input type="password" placeholder="Confirm Wallet Password" name='confirmEtherWalletPassword' onChange={this.pageValidation} required />
                                        {this.state.errors.confirmEtherWalletPassword &&
                                            <div className="form__field" style={{ color: 'red' }}>{this.state.errors.confirmEtherWalletPassword}</div>
                                        }
                                    </div>
                                   

                                    <div className="form-group">
                                        <div className="terms-condition">
                                            <input type="checkbox" checked={this.state.isChecked} onChange={this.termsConditions} required /> Do you agree to the
                                    <a href="javascript:void(0);"> Terms and conditions?</a>
                                            {
                                                this.state.errors.isChecked &&
                                                <div className="form__field" style={{ color: 'red' }}>{this.state.errors.isChecked}</div>
                                            }
                                        </div>
                                    </div>



                                    <div className="form-group">
                                        <button type="button" className="btn btn-login" disabled={this.state.userName === '' || this.state.emailId === '' || this.state.password === '' || this.state.mobileNo === '' || this.state.confirmPassword === '' || this.state.etherWalletPassword === '' || !this.state.isChecked} onClick={this.handleClick}>Register</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <Loader data={this.state.loading} />

            </div>
        )
    }

}

export default Register;