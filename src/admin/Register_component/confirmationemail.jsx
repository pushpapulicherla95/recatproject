import React from 'react'
import '../../../public/css/style.css';
import { API_BASE_URL } from '../../constant/baseurl.js';
import queryString from 'query-string';
import axios from 'axios';
import Notifications, { notify } from 'react-notify-toast';
import { NavLink } from 'react-router-dom';

class Confirmationmail extends React.Component {
    constructor(props) {
        super(props)
        const parsed = queryString.parse(props.location.search);
        this.state = {
            confMailid: parsed.emailId
        }

    }
    componentWillMount() {
        if (this.state.confMailid) {
            const val = this;
            this.setState({ loading: true });
            let tokenUrl = API_BASE_URL + "artcoin/api/user/emailverification";

            let payload = {
                emailId: this.state.confMailid
            }
            axios.post(tokenUrl, payload)
                .then(response => {

                    console.log("confirmation email res>>>", response);
                    if (response.status == 200) {
                        notify.show(response.data.message, 'success');
                    }
                    else {
                        if (response.data.message == 'Session Expired') {
                            val.props.history.push('/login');
                        }
                        notify.show(response.data.message, "error");
                        this.setState({ test: false });
                        this.setState({ loading: false });
                        self.setState({ errorResponse: response.data.message });
                        setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false });
                    self.setState({ errorResponse: error.message });
                    setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                });
        }

    }
    render() {
        return (
            <div className="login-fullwrap">
                 <Notifications />

                <div className="pos-clicklogin"> 
                <NavLink to={'/login'}> 

                   <button type="button" className="btn btn-login" >Click here to login </button> </NavLink>

                </div>
            </div>
        )
    }
}
export default Confirmationmail;