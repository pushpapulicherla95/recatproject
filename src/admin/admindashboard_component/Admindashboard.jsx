import React from 'react';
import { NavLink } from 'react-router-dom';

import '../../../public/css/style.css';
import '../../../public/css/responsive.css';
import '../../../public/css/reset.css';
import '../../../public/css/font-awesome.css';
import '../../../public/css/animate.css';

import { API_BASE_URL } from '../../constant/baseurl.js';
import validator from 'validator';
import Loader from '../../constant/loader.js';
import Notifications, { notify } from 'react-notify-toast';
import { CopyToClipboard } from 'react-copy-to-clipboard';


import Header from '../../common/Header';
import Sidebar from '../../common/Sidebar';
import axios from 'axios';

class Admindashboard extends React.Component {
  constructor(props) {
    super(props);
    let abc = JSON.parse(sessionStorage.getItem('userData'));
    console.log("Admin userdata abc>>>", abc);
    this.state = {
      data: abc,
      sessionId: '',
      showPopup: false,
      noOfToken: '',
      tokenBalance: '',
      toAddress: '',
      amount: '',
      errorResponse: '',
      errors: {},
      loading: false,
      transactionHistory: [],
      bitcoinWalletAddress: abc.bitcoinWalletReceivingAddress,
      etherWalletAddress: abc.etherWalletAddress,
      etherBalance: '',
      bitcoinBalance: '',
      numberOfUsers:'',
      burnedTokens:'',
      soldTokens:''

    }
    if (sessionStorage.getItem('userData') == null) {
      props.history.push('/login');
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.serviceHandler = this.serviceHandler.bind(this);
    this.togglePopup = this.togglePopup.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onCopy = this.onCopy.bind(this);
    this.bitcoinEtherBal = this.bitcoinEtherBal.bind(this);
    this.userTokencombined = this.userTokencombined.bind(this);
    this.tokenBalance = this.tokenBalance.bind(this);
    


    this.bitcoinEtherBal();
    this.userTokencombined();
    this.tokenBalance();
  }

  tokenBalance() {
    if (sessionStorage.getItem('userData') != null) {
      const props = this.props;
      let abc = JSON.parse(sessionStorage.getItem('userData'));
      const tokenUrl = API_BASE_URL + "artcoin/token/api/token/balance";

      this.setState({ loading: false });

      let sessionValu = {
        "sessionId": abc.sessionId
      }
      axios.post(tokenUrl, sessionValu)
        .then(response => {
          if (response.status == 200) {
            this.setState({ loading: false });
            this.setState({ tokenBalance: response.data.TokenBalanceInfo.tokenBalance });
            sessionStorage.setItem('tokenvalue', JSON.stringify(response.data.TokenBalanceInfo));
            if (res.data.loginInfo.roleId = 1) {
              props.history.push('/admindashboard');

            } else if (res.data.loginInfo.roleId = 2) {
              props.history.push('/userdashboard');
            }

          }
          else {
            if (response.data.message == 'Session Expired') {
              props.history.push('/login');
            }
            notify.show(response.data.message, "error");
            
            this.setState({ loading: false });
            self.setState({ errorResponse: response.data.message });
            setTimeout(() => self.setState({ errorResponse: '' }), 5000);

          }
        })
        .catch(function (error) {
          this.setState({ loading: false });
          self.setState({ errorResponse: error.message });
          setTimeout(() => self.setState({ errorResponse: '' }), 5000);

        });
    }
  }

  userTokencombined() {
    const tokenUrl = API_BASE_URL + "artcoin/api/Token/Details";
    
    axios.post(tokenUrl)
      .then(response => {

        console.log("combined api response>>>", response);
        if (response.status == 200) {
          this.setState({ test: false });
          this.setState({ loading: false, numberOfUsers: response.data.loginInfo.numberOfUsers ,soldTokens:response.data.loginInfo.soldTokens,burnedTokens:response.data.loginInfo.burnedTokens});
        }
        else {
          if (response.data.message == 'Session Expired') {
            props.history.push('/login');
          }
          notify.show(response.data.message, "error");
          this.setState({ loading: false });
          self.setState({ errorResponse: response.data.message });
          setTimeout(() => self.setState({ errorResponse: '' }), 5000);

        }
      })
      .catch(function (error) {
        this.setState({ loading: false });
        self.setState({ errorResponse: error.message });
        setTimeout(() => self.setState({ errorResponse: '' }), 5000);

      });
  }
  bitcoinEtherBal() {
    console.log("testtt>>>");
    const tokenUrl = API_BASE_URL + "artcoin/api/user/ether/balance";
    let payload = {
      "sessionId": this.state.data.sessionId
    }
    const props = this.props;
    axios.post(tokenUrl, payload)
      .then(response => {

        console.log("eth balance response>>>", response);
        if (response.status == 200) {
          this.setState({ test: false });
          this.setState({ loading: false, etherBalance: response.data.bitcoinBalanceInfo.etherBalance });
        }
        else {
          if (response.data.message == 'Session Expired') {
            props.history.push('/login');
          }
          notify.show(response.data.message, "error");

          this.setState({ test: false });
          this.setState({ loading: false });
          self.setState({ errorResponse: response.data.message });
          setTimeout(() => self.setState({ errorResponse: '' }), 5000);

        }
      })
      .catch(function (error) {
        this.setState({ test: false });
        this.setState({ loading: false });
        self.setState({ errorResponse: error.message });
        setTimeout(() => self.setState({ errorResponse: '' }), 5000);

      });

    if (this.state.data.sessionId) {
      const props = this.props;
      const tokenUrl = API_BASE_URL + "artcoin/api/user/bitcoin/balance";
      let payload = {
        "sessionId": this.state.data.sessionId
      }

      axios.post(tokenUrl, payload)
        .then(response => {

          console.log("bit balance response>>>", response);
          if (response.status == 200) {
            this.setState({ test: false });
            this.setState({ loading: false, bitcoinBalance: response.data.bitcoinBalanceInfo.bitcoinBalance });
          }
          else {
            if (response.data.message == 'Session Expired') {
              props.history.push('/login');
            }
            notify.show(response.data.message, "error");

            this.setState({ test: false });
            this.setState({ loading: false });
            self.setState({ errorResponse: response.data.message });
            setTimeout(() => self.setState({ errorResponse: '' }), 5000);

          }
        })
        .catch(function (error) {
          this.setState({ test: false });
          this.setState({ loading: false });
          self.setState({ errorResponse: error.message });
          setTimeout(() => self.setState({ errorResponse: '' }), 5000);

        });
    }


  }
  onCopy() {
    console.log("in side copy");
    this.setState({ copied: true });
    notify.show('Successfully copied!', 'success');
  };
  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });

  }
  handleChange(event) {
    event.preventDefault();
    this.setState({ [event.target.name]: event.target.value });

  }



  validateField(data) {
    const errors = {};
    if (validator.isEmpty(data.toAddress)) {
      errors.toAddress = "Please enter the wallet address"
    } else {
      errors.toAddress = ''
    }

    if (validator.isEmpty(data.amount)) {
      errors.amount = "Please enter the number of tokens"
    } else {
      errors.amount = ''
    }
    if (!validator.isNumeric(data.amount)) {
      errors.amount = 'Please Enter Valid Tokens'
    }

    return errors;
  }
  validateTokendata(payload) {
    let errors = {};
    if (validator.isEmpty(payload.noOfToken)) {
      errors.noOfToken = "Please Enter Valid Tokens"
    } else {
      errors.noOfToken = ''
    }

    if (!validator.isNumeric(payload.noOfToken)) {
      errors.noOfToken = 'Please Enter Valid Tokens'
    }



    return errors;
  }
  handleSubmit(event) {
    const apiBaseUrl = API_BASE_URL + "artcoin/token/api/delete/token";
    var payload = {
      "sessionId": this.state.data.sessionId,
      "noOfToken": this.state.noOfToken
    }
    let errors = this.validateTokendata(payload);
    if (errors.noOfToken == '') {
      this.setState({ loading: true });
      axios.post(apiBaseUrl, payload)
        .then(response => {
          console.log("burn token responseeeee>>>>", response, response.data.loginInfo.burnedTokens);
          if (response.status == 200) {
            this.setState({
              burnedTokens: response.data.loginInfo.burnedTokens
            });
            this.setState({
              loading: false,
              showPopup: !this.state.showPopup
            });

            // this.setState({ loading: false });
            // this.setState({
            //   showPopup: !this.state.showPopup
            // });
            notify.show(response.data.message, 'success');
            this.userTokencombined();
            this.tokenBalance();

          } else {
            notify.show(response.data.message, 'error');
            this.setState({
              showPopup: !this.state.showPopup
            });
            this.setState({ loading: false });
            self.setState({ errorResponse: response.data.message });
            setTimeout(() => self.setState({ errorResponse: '' }), 5000);
          }

        })
        .catch(function (error) {
          this.setState({ loading: false });

          self.setState({ errorResponse: error.message });
          setTimeout(() => self.setState({ errorResponse: '' }), 5000);

        });

    }
    else {
      this.setState({ errors });
      setTimeout(() => this.setState({ errors: '' }), 5000);
    }



  }




  handleClick(event) {
    event.preventDefault();
    const props = this.props;
    let payload = {
      "sessionId": this.state.data.sessionId,
      "toAddress": this.state.toAddress,
      "amount": this.state.amount

    }
    let errors = this.validateField(payload);
    if (errors.toAddress == '' && errors.amount == '') {
      this.serviceHandler(payload);
    }
    else {
      this.setState({ errors });
      setTimeout(() => this.setState({ errors: '' }), 5000);
    }



  }
  serviceHandler(payload) {
    const apiBaseUrl = API_BASE_URL + "artcoin/token/api/token/transfer";
    this.setState({ loading: true });
    const props = this.props;
    axios.post(apiBaseUrl, payload)
      .then(response => {
        console.log("token transfer>>", response);
        if (response.status == 200) {
          this.setState({ loading: false });

          this.setState({ toAddress: '' });
          this.setState({ amount: '' });

          notify.show(response.data.message, "success");

          props.history.push('/admindashboard');
          this.tokenBalance();
          
        } else {
          if (response.data.message == 'Session Expired') {
            props.history.push('/login');
          }
          this.setState({ loading: false });
          notify.show(response.data.message, "error");
          self.setState({ errorResponse: response.data.message });
          setTimeout(() => self.setState({ errorResponse: '' }), 5000);
        }

      })
      .catch(function (error) {
        self.setState({ errorResponse: error.message });
        setTimeout(() => self.setState({ errorResponse: '' }), 5000);

      });
  }
  render() {
    return (
      <div id="container">
        <Header propsdata={this.props} />
        <Sidebar />
        <div id="main-content">
          <div className="wrapper">
            <div className="dashboard-title">
              <h1>Admin Dashboard</h1>
            </div>

            <div className="admin-list-box">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-12 col-xs-12 col-sm-12">
                    <div className="menchor-wallet-id">
                      <p>Bitcoin Address :
                                {this.state.data != null && <span>{this.state.data.bitcoinWalletReceivingAddress}</span>}
                      </p>
                      <div className="copyclip"><CopyToClipboard onCopy={this.onCopy} text={this.state.bitcoinWalletAddress}>
                        <span>Copy</span>
                      </CopyToClipboard></div>
                    </div>
                    <div className="menchor-wallet-id">
                      <p>Ether Address :
                                 {this.state.data != null && <span>{this.state.data.etherWalletAddress}</span>}
                      </p>
                      <div className="copyclip"><CopyToClipboard onCopy={this.onCopy} text={this.state.etherWalletAddress}>
                        <span>Copy</span>
                      </CopyToClipboard></div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-3 col-sm-6">
                    <div className="list-box-4 animated hiding" data-animation="zoomIn">
                      <div className="widget-panel bg-pink">
                        <div className="pos-img"><img src="public/image/user-2.png" /></div>
                        {this.state.data != null && <h2 className="counter">{this.state.numberOfUsers}</h2>}
                        <div className="visit-list">Users</div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                    <div className="list-box-4 animated hiding" data-animation="zoomIn">
                      <div className="widget-panel bg-purple">
                        <div className="pos-img"><img src="public/image/ether.png" /></div>
                        {this.state.etherBalance != null && <h2 className="counter">{this.state.etherBalance}</h2>}
                        <div className="visit-list">Ether Balance</div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                    <div className="list-box-4 animated hiding" data-animation="zoomIn">
                      <div className="widget-panel bg-info">
                        <div className="pos-img"><img src="public/image/bitcoin-bal.png" /></div>
                        {this.state.bitcoinBalance != null && <h2 className="counter">{this.state.bitcoinBalance}</h2>}
                        <div className="visit-list">Bitcoin Balance</div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                    <div className="list-box-4 animated hiding" data-animation="zoomIn">
                      <div className="widget-panel bg-success">
                        <div className="pos-img"><img src="public/image/diamond.png" /></div>
                        <h2 className="counter">{this.state.data.icoTokens}</h2>
                        <div className="visit-list">ICO Tokens</div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                    <div className="list-box-4 animated hiding" data-animation="zoomIn">
                      <div className="widget-panel bg-red">
                        <div className="pos-img"><img src="public/image/toal-icon.png" /></div>
                        <h2 className="counter">{this.state.data.initialTokens}</h2>
                        <div className="visit-list">Total Tokens</div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                    <div className="list-box-4 animated hiding" data-animation="zoomIn">
                      <div className="widget-panel bg-dpink">
                        <div className="pos-img"> <img src="public/image/diamond.png" /></div>
                        <h2 className="counter">{this.state.soldTokens}</h2>
                        <div className="visit-list">ICO Sold Tokens</div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                    <div className="list-box-4 animated hiding" data-animation="zoomIn">
                      <div className="widget-panel bg-blue">
                        <div className="pos-img"> <img src="public/image/bal-token.png" /></div>
                        {this.state.tokenBalance && <h2 className="counter">{this.state.tokenBalance}</h2>}
                        <div className="visit-list">Balance Tokens</div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                    <div className="list-box-4 animated hiding" data-animation="zoomIn">
                      <div className="widget-panel bg-orange widget-last-box">
                        <div className="pos-img"> <img src="public/image/delete.png" onClick={this.togglePopup.bind(this)} /></div>
                        <h2 className="counter">{this.state.burnedTokens}</h2>
                        <div className="visit-list">Burn Tokens</div>
                        {/* <button type="button" onClick={this.togglePopup.bind(this)} className="visit-list">Burn Unsold Tokens</button> */}

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Notifications />

            <div className="coin-transfer">
              <div className="container-fluid">
                <div className="col-md-12 col-sm-12 col-xs-12 coin-transfer-padd">
                  <div className="transaction-title">
                    <h1>Token Transfer</h1>
                  </div>
                  <div className="coin-transfer-wrap">
                    <form className="adjust_padding">
                      <div className="form-group">
                        <label for="wallet"> Wallet Address</label>
                        <input name="wallet-address" value={this.state.toAddress} type="text" name='toAddress' onChange={this.handleChange} className="form-control" />
                        {this.state.errors.toAddress &&
                          <div style={{ color: 'red', paddingTop: '5px' }}>{this.state.errors.toAddress}</div>
                        }
                      </div>
                      <div className="form-group">
                        <label for="wallet"> Number of Tokens</label>
                        <input name="wallet-address" value={this.state.amount} type="text" name='amount' onChange={this.handleChange} className="form-control" />
                        {this.state.errors.amount &&
                          <div style={{ color: 'red', paddingTop: '5px' }}>{this.state.errors.amount}</div>
                        }
                      </div>
                      {
                        this.state.errorResponse &&
                        <div className="login-errormsg m_top1 alert alert-danger" style={{ color: 'red' }}><h6 className="login-msg-h6"><b>{this.state.errorResponse}</b></h6>
                        </div>
                      }
                      <div className="form-group send-btn-bttm">
                        <button type="button" className="btn btn-send" onClick={this.handleClick}>Send</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Loader data={this.state.loading} />

        {this.state.showPopup ?
          <Popup
            text='Close Me' data={this.state}
            closePopup={this.togglePopup.bind(this)}
            handle={this.handleChange.bind(this)}
            submit={this.handleSubmit}
          />
          : null
        }


      </div>

    )
  }

}

class Popup extends React.Component {
  render() {
    console.log("this.state>>>>>>>>>>>>>>>>>", this.props.data);
    return (
      <div>
        <div>
          <div className='popup'>
            <div className='popup_inner popup_inner-burn'>
              <h1>Burn Token</h1>
              <div className="popup_inner_body">
                <label>Tokens:</label>
                <input type='text' name='bitcoinToken' name='noOfToken' onChange={this.props.handle} />
                {this.props.data.errors.noOfToken &&
                  <div style={{ color: 'red', paddingTop: '5px' }}>{this.props.data.errors.noOfToken}</div>
                }
                <div className="bttn"> <button onClick={this.props.submit}>submit</button>
                  <button onClick={this.props.closePopup}>Cancel</button>
                </div>
                <Loader data={this.props.data.loading} />

              </div>
            </div>
          </div>
        </div>
        <div>

        </div>
      </div>
    );
  }
}
export default Admindashboard;