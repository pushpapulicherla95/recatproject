import React from 'react';
import { NavLink } from 'react-router-dom';
import validator from 'validator';
import { API_BASE_URL } from '../../constant/baseurl.js';

import '../../../public/css/style.css';
import '../../../public/css/responsive.css';
import '../../../public/css/reset.css';
import '../../../public/css/font-awesome.css';
import Loader from '../../constant/loader.js';
import Notifications, { notify } from 'react-notify-toast';
import queryString from 'query-string';
import axios from 'axios';


class Forgotpassword extends React.Component {

    constructor(props) {
        super(props)
        const parsed = queryString.parse(props.location.search);
        console.log("parsed?>>>",parsed);
        this.state = {
            emailId: '',
            errors: {},
            loading: false,
            verificationemailId: parsed.emailId,
            token: parsed.token
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);

    }

    componentWillMount() {
        if (this.state.verificationemailId && this.state.token) {
            const val = this;
            const props = this.props;
            this.setState({ loading: true });
            let tokenUrl = API_BASE_URL + "artcoin/api/forgot/password/linkVerification";

            let payload = {
                emailId: this.state.verificationemailId,
                token: this.state.token
            }
            axios.post(tokenUrl, payload)
                .then(response => {

                    console.log("paswdlink verification>>>", response);
                    if (response.status == 200) {
                        console.log("props.history");
                        sessionStorage.setItem('verificationdata', JSON.stringify(response.data.bitcoinBalanceInfo));
                        props.history.push('/resetPassword');
                    }
                    else {
                        if (response.data.message == 'Session Expired') {
                            props.history.push('/login');
                        }
                        notify.show(response.data.message, "error");
                        this.setState({ test: false });
                        this.setState({ loading: false });
                        self.setState({ errorResponse: response.data.message });
                        setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false });
                    self.setState({ errorResponse: error.message });
                    setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                });
        }

    }
    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
    }

    handleClick(event) {
        event.preventDefault();
        const props = this.props;
        let errors = {};
        let payload = {
            "emailId": this.state.emailId
        }
        const apiBaseUrl = API_BASE_URL + "artcoin/api/forgot/password";
        if (!validator.isEmail(this.state.emailId)) {
            errors.emailId = 'Please enter valid email address';
            this.setState({ errors });
            setTimeout(() => this.setState({ errors: '' }), 2000);

        } else {
            this.setState({ loading: true });
            axios.post(apiBaseUrl, payload)
                .then(response => {
                    console.log("forgetpassword result", response);
                    if (response.status == 200) {
                        console.log("propssss", props);
                        this.setState({ loading: false });
                        props.history.push('/login');
                        notify.show(response.data.message, "success");
                    }
                    else {
                        if (response.data.message == 'Session Expired') {
                            props.history.push('/login');
                        }
                        this.setState({ loading: false });
                        notify.show(response.data.message, "error");
                        self.setState({ errorResponse: response.data.message });
                        setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false });
                    console.log("catche", error);
                    self.setState({ errorResponse: error.message });
                    setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                });

        }



    }
    render() {
        console.log("this.props>>>>>>>>", this.props);
        return (
            <div className="login-fullwrap">
                <Notifications />

                <div className="login-section forgot-section">
                    <div className="login-body">
                        <form>
                            <div className="form-logo txt-center">
                                <img src="public/image/logo.png" alt="logo" />
                            </div>
                            <div className="form-heading txt-center">

                                <div className="form-title">Forgot Password</div>
                                <div className="login-form forgot-bttm">

                                    <div className="form-group">
                                        <input type="text" name='emailId' placeholder="Enter Your Email" onChange={this.handleChange} required />
                                        {this.state.errors.emailId &&
                                            <div className="form__field" style={{ color: 'red', paddingTop: '5px' }}>{this.state.errors.emailId}</div>
                                        }
                                    </div>
                                    <div className="form-group">
                                        <button type="button" className="btn btn-login btn-forgot" onClick={this.handleClick}>Send Mail</button>
                                    </div>

                                </div>
                                <div className="form-group">
                                    <p className="account-reg">Dont’t have an Account?
                                    <NavLink to={'/register'}> Register Now</NavLink>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <Loader data={this.state.loading} />
            </div>
        );
    }
}

export default Forgotpassword;