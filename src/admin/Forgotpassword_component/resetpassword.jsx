import React from 'react';
import '../../../public/css/style.css';
import axios from 'axios';
import { API_BASE_URL } from '../../constant/baseurl.js';
import validator from 'validator';
import Notifications, { notify } from 'react-notify-toast';
import Loader from '../../constant/loader.js';

class Resetpassword extends React.Component {
    constructor(props) {
        super(props);
        let abc = JSON.parse(sessionStorage.getItem('verificationdata'));
        console.log("abc", abc);

        this.state = {
            password: '',
            confirmPassword: '',
            errors: {},
            emailId: abc.emailId,
            token:abc.token,
            payload: {},
            loading:false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.isEmpty = this.isEmpty.bind(this);
        this.submitValidations = this.submitValidations.bind(this)

    }
    submitValidations(value) {
        const errors = {};
        if (value.password) {

            if (!validator.isLength(value.password, 5, 19)) {
                errors.password = 'Length should be 6-20 characters';

            }
        }

        if (value.password && value.confirmPassword != '') {
            if (value.confirmPassword != value.password) {
                errors.confirmPassword = 'Password does not match';
            }
        }
        if (value.confirmPassword) {
            if (!validator.equals(value.confirmPassword, value.password)) {
                errors.confirmPassword = 'Password does not match';
            }

        }
        this.setState({ errors });
        setTimeout(() => this.setState({ errors: '' }), 2000);
        return errors;
    }

    isEmpty(obj) {

        if (obj == null) return true;
        if (obj.length > 0) return false;
        if (obj.length === 0) return true;
        if (typeof obj !== "object") return true;
        for (var x in obj) {
            return false;
        }

        return true;

    }

    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
    }
    handleClick(event) {
        event.preventDefault();
        console.log("dfdfdf", this.state);
        const props = this.props;
        this.state.payload = {
            "emailId": this.state.emailId,
            "password": this.state.password,
            "confirmPassword": this.state.confirmPassword,
            "token":this.state.token
        }
        let validationerr = this.submitValidations(this.state.payload);
        if (this.isEmpty(validationerr)) {
            const apiBaseUrl = API_BASE_URL + "artcoin/api/forgot/password/reset";
            this.setState({ errors: '' });
            this.setState({ loading: true });
          
            axios.post(apiBaseUrl, this.state.payload)
            .then(response => {
                console.log("reset res verification>>>", response);
                if (response.status == 200) {
                        setTimeout(() => {props.history.push('/login')
                        notify.show(response.data.message, 'success');
                        this.setState({ loading: false });
                    }, 400);
                        
                   
                }
                else {
                    if (response.data.message == 'Session Expired') {
                        props.history.push('/login');
                    }
                    notify.show(response.data.message, "error");
                    this.setState({ test: false });
                    this.setState({ loading: false });
                    self.setState({ errorResponse: response.data.message });
                    setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                }
            })
            .catch(function (error) {
                this.setState({ loading: false });
                self.setState({ errorResponse: error.message });
                setTimeout(() => self.setState({ errorResponse: '' }), 5000);

            });
        }


    }
    render() {
        return (
            <div className="reset-password-sig">
                <div className="reset-pass">
                    <div className="reset-head">
                        <h1>Reset Password</h1>
                    </div>
                   
                    <div className="reset-pass-body">
                    <Notifications />

                        <form>
                            <div className="form-group">
                                <input type="password" name="password" onChange={this.handleChange} placeholder="New Password" />
                                {this.state.errors.password &&
                                    <div className="form-div-error">{this.state.errors.password}</div>
                                }
                            </div>
                            <div className="form-group">
                                <input type="password" name="confirmPassword" onChange={this.handleChange} placeholder="Confirm Password" />
                                {this.state.errors.confirmPassword &&
                                    <div className="form-div-error">{this.state.errors.confirmPassword}</div>
                                }
                            </div>
                            <div className="submit-bttn-form">
                                <button type="button" className="btn btn-resent" disabled={this.state.password === '' || this.state.confirmPassword === ''} onClick={this.handleClick} >Submit</button>
                            </div>
                        </form>
                        <Loader data={this.state.loading} />
                    </div>
                </div>
            </div>
        );
    }
}
export default Resetpassword;