import React from 'react';
import Header from '../../common/Header';
import Sidebar from '../../common/Sidebar';
import { API_BASE_URL } from '../../constant/baseurl.js';
import axios from 'axios';
import Pagination from 'react-js-pagination';
import Notifications, { notify } from 'react-notify-toast';

    class Userlist extends React.Component {
        
        constructor(props) {
            let abc = JSON.parse(sessionStorage.getItem('userData'));
            super(props);
            this.state = {
                data:abc,
                userlist: [],
                page: 1,
                perPage:10,
                
            }
            this.handlePageChange= this.handlePageChange.bind(this);
        }
        componentWillMount() {

            if (sessionStorage.getItem('userData') != null) {
                const props = this.props;
                
                const listurl = API_BASE_URL + "artcoin/api/list/users";
                console.log("this.sttt willl",this.state);
                let sessionValue = {
                    "sessionId": this.state.data.sessionId,
    
                }
                this.setState({ loading: true });
                axios.post(listurl, sessionValue)
                    .then(response => {
                        console.log("total user list>>>", response);
    
                        console.log("mmm", response.data.listToken);
                        console.log("type", typeof response.data.listToken);
                        if (response.status == 200) {
                            // sessionStorage.setItem('tokenhistroy', JSON.stringify(response.data.listToken));
                            this.setState({ userlist: response.data.listUsers });
                            

                        }
                        else {
                            this.setState({ loading: false });
                            if (response.data.message == 'Session Expired') {
                                props.history.push('/login');
                            }
                            notify.show(response.data.message, "error");
                            self.setState({ errorResponse: response.data.message });
                            setTimeout(() => self.setState({ errorResponse: '' }), 5000);
    
                        }
                    })
                    .catch(function (error) {
                        this.setState({ loading: false });
                        console.log("catche", error);
                        self.setState({ errorResponse: error.message });
                        setTimeout(() => self.setState({ errorResponse: '' }), 5000);
    
                    });
            }
        }
        
        handlePageChange(page){
            this.setState({page})
            console.log('evt', this.state.page)
        }
    render() {
        return (
            <div id="container">
                <Header propsdata={this.props} />
                <Sidebar />
                <div id="main-content">
                    <div className="wrapper">
                        <div className="dashboard-title">
                            <h1>User List</h1>
                        </div>
                        <Notifications />

                        <div class="user-board-fullwrap data-menchorcoin referral-link">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive">
                                    {this.state.userlist.length > 0 ?
                                        <table class="table">
                                            <thead className="text-cent">
                                                <th>S.No</th>
                                                <th>Email</th>
                                                <th>Reference Id</th>
                                                <th>Date</th>
                                                <th>Status</th>

                                            </thead>
                                            <tbody className="text-cent">
                                                {
                                                    this.state.userlist.map((user, key) =>
                                                        this.state.page * this.state.perPage > key
                                                         && (this.state.page -1) * this.state.perPage <= key 
                                                         && <tr key={key}>
                                                         <td >{key + 1}</td>
                                                                <td >{user.emailId}</td>
                                                                <td>{user.referenceId}</td> 
                                                                <td>{user.date}</td>
                                                                {user.activation ?
                                                                <td><button className="btn green">Active</button></td>:
                                                                <td><button className="btn red">InActive</button></td>
                                                                }
                                                        </tr>
                                                    )
                                                }
                                            </tbody>
                                        </table>
                                        :
                                        <table>
                                            <thead>No user list</thead>
                                        </table>

                                    }
                                    
                                    <div class="clearfix"></div>
                                </div>  
                            </div>
                        </div>
                        <div>
                        {this.state.userlist.length>0 &&
                        <Pagination
                            activePage={this.state.page}
                            itemsCountPerPage={this.state.perPage}
                            totalItemsCount={this.state.userlist.length}
                            pageRangeDisplayed={5}
                            onChange={this.handlePageChange}/>
                        }
                                                </div>

                    </div>
                </div>

            </div>)
    }
}
export default Userlist;