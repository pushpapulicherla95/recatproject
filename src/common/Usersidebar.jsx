import React from 'react';
import { NavLink } from 'react-router-dom';
import { GoogleLogout } from 'react-google-login';
import '../../public/css/style.css';

class Sidebar extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            tabName: ''
        }
        // let googleUser = JSON.parse(sessionStorage.getItem("GoogleUser"));
        // console.log("googleUSer ====", googleUser);
        // const facebookUserSession = JSON.parse(sessionStorage.getItem('FacebookUser'));
        // console.log("facebookUSer ====", facebookUserSession);
    }
    
    render() {
        const menuItems = [{
            path: '/userdashboard',
            img_path: 'public/image/dashboard.png',
            item: 'Dashboard'
        },
        {
            path: '/usertranscation',
            img_path: 'public/image/transaction.png',
            item: 'My Transactions'
        },
        // {
        //     path: '/Support',
        //     img_path: 'public/image/settings.png',
        //     item: 'Support'
        // },
        {
            path: '/Refferal',
            img_path: 'public/image/referral.png',
            item: 'Received Refferal'
        }];
        return (
            <aside>
                <div id="sidebar" className="nav-collapse">
                    <div className="leftside-navigation">
                        <ul className="sidebar-menu" id="nav-accordion">
                            <li className="nav-profile">
                                <img src="public/image/logo-1.png" /> </li>

                                {menuItems.map((item, index)=> {
                                    return(
                                    <li key ={index} >
                                        <NavLink to={item.path}  activeStyle={{background:'rgba(86, 108, 144, 1)',color:'#fff'}}>
                                            <img src={item.img_path} />
                                            <span className="m_left">{item.item}</span>
                                        </NavLink>
                                    </li>
                                
                                )})}
                            {/* <li>
                                <NavLink className="active" to={'admindashboard'}>
                                    <img src="public/image/dashboard.png" />
                                    <span className="m_left">Dashboard</span>
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to={'admintranscation'}>
                                    <img src="public/image/transaction.png" />
                                    <span className="m_left">My Transaction</span>
                                </NavLink>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="public/image/settings.png" />
                                    <span>Support</span>
                                </a>
                            </li> */}
                        </ul>
                    </div>
                </div>
            </aside>
        )
    }
}

export default Sidebar;

