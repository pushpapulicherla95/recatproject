import React from 'react';
import '../../public/css/style.css';
import { API_BASE_URL } from '../constant/baseurl.js';
import validator from 'validator';
import Notifications, { notify } from 'react-notify-toast';
import Loader from '../constant/loader.js';

import axios from 'axios';

class Header extends React.Component {
    constructor(props) {
        super(props);
        let abc = JSON.parse(sessionStorage.getItem('userData'))
        this.state = {
            data: abc,
            password: '',
            confirmPassword: '',
            errors: {},
            googleSession: {},
            facebookSession: {},
            test: true,
            loading: false,
            payload:{}
        };

        this.logout = this.logout.bind(this);
        this.isEmpty = this.isEmpty.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.buttonClick = this.buttonClick.bind(this);
        this.submitValidations = this.submitValidations.bind(this)

    }
    buttonClick(event) {
        event.preventDefault();
        this.setState({ password: '' });
        this.setState({ confirmPassword: '' });

    }
    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
    }
    isEmpty(obj) {

        if (obj == null) return true;
        if (obj.length > 0) return false;
        if (obj.length === 0) return true;
        if (typeof obj !== "object") return true;
        for (var x in obj) {
            return false;
        }

        return true;

    }
    submitValidations(value) {
        const errors = {};
        if (value.password) {
           
            if (!validator.isLength(value.password, 5, 19)) {
                errors.password = 'Length should be 6-20 characters';

            } 
        }

        if (value.password && value.confirmPassword != '') {
            if (value.confirmPassword != value.password) {
                errors.confirmPassword = 'Password does not match';
            }
        }
        if (value.confirmPassword) {
            if (!validator.equals(value.confirmPassword, value.password)) {
                errors.confirmPassword = 'Password does not match';
            }
          
        }
        this.setState({ errors });
        setTimeout(() => this.setState({ errors: '' }), 2000);
        return errors;
    }
   
    handleClick(event) {
        event.preventDefault();
        let errors = this.state.errors;
        this.state.payload = {
            "password": this.state.password,
            "confirmPassword": this.state.confirmPassword,
            "sessionId": this.state.data.sessionId
        }

        let validationerr = this.submitValidations(this.state.payload);
        console.log("validationerr>>>>>",validationerr);
        console.log("this.isEmpty(validationerr",this.isEmpty(validationerr));
        if (this.isEmpty(validationerr)) {
            console.log("abc");
            this.setState({ errors: '' });
            this.handlerService(this.state.payload);
        }
       
    }

    handlerService(payload) {
        const apiBaseUrl = API_BASE_URL + "/artcoin/api/user/reset/password";
        var self = this;
        this.setState({ loading: true });
        axios.post(apiBaseUrl, payload)
            .then(response => {
                if (response.status == 200) {
                    this.setState({ loading: false ,test:false});
                    setTimeout(() => self.setState({ test:true,password:'',confirmPassword:'' }), 200);
                    notify.show(response.data.message, 'success');
                }
                else {
                    this.setState({ test: false });
                    this.setState({ loading: false });

                    notify.show(response.data.message, 'error');

                    self.setState({ errorResponse: response.data.message });
                    setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                }
            })
            .catch(function (error) {
                this.setState({ test: false });
                self.setState({ errorResponse: error.message });
                setTimeout(() => self.setState({ errorResponse: '' }), 5000);

            });
    }
    logout(event) {
        event.preventDefault();
        const props = this.props.propsdata;
        console.log("Logout", props);
        const apiBaseUrl = API_BASE_URL + "artcoin/api/logout";
        this.setState({ loading: true });
        // const { props } = this.props;
        // console.log("");
        // props.history.push('/login');
        let payload = {
            "sessionId": this.state.data.sessionId
        }
        axios.post(apiBaseUrl, payload)
            .then(response => {
                console.log("log out>>>>", response);
                if (response.status == 200) {
                    this.setState({ loading: false });
                    sessionStorage.removeItem("userData");
                    sessionStorage.removeItem("tokenvalue");

                    // this.setState({ test: false });
                    props.history.push('/login');
                    notify.show(response.data.message, 'success');

                }
                else {
                    if (response.data.message == 'Session Expired') {
                        props.history.push('/login');
                    }
                    this.setState({ loading: false });
                    this.setState({ test: false });
                    notify.show(response.data.message, "error");
                    self.setState({ errorResponse: response.data.message });
                    setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                }
            })
            .catch(function (error) {
                this.setState({ test: false });
                this.setState({ loading: false });
                console.log("catche", error);
                self.setState({ errorResponse: error.message });
                setTimeout(() => self.setState({ errorResponse: '' }), 5000);

            });

    }

    render() {
        return (
            <div className="header fixed-top clearfix">
                <div className="brand">
                    <a href="#" className="logo"> All of Art Token </a>
                    {/* <div className="sidebar-toggle-box">
                        <div className="fa fa-bars"></div>
                    </div> */}
                </div>
                <div className="top-nav clearfix">
                    <ul className="nav pull-right top-menu">
                        <li className="dropdown">
                            <a data-toggle="dropdown" className="dropdown-toggle" href="#">
                                <img alt="" src="public/image/user.png" /> </a>
                            <ul className="dropdown-menu extended logout">
                                <li>
                                    <a href="" data-toggle="modal" data-target="#resetpwd">
                                        <i className="fa fa-cog"></i> Reset Password</a>
                                </li>
                                <li>
                                    <a onClick={this.logout} href="javascript:void(0);">
                                        <i className="fa fa-key"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <Notifications />
                {(this.state.test || this.state.test) && <div className="reset-password-wrap">
                    <div className="modal fade" id="resetpwd" role="dialog">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" onClick={this.buttonClick} data-dismiss="modal">&times;</button>
                                    <h4 className="modal-title">Reset Password</h4>
                                </div>
                                <div className="modal-body">
                                    <div className="reset-password">
                                        <form action="" method="post">
                                            <div className="form-group">
                                                <input type="password" value={this.state.password} name='password' onChange={this.handleChange} placeholder="New Password" />
                                                {this.state.errors.password &&
                                                    <div className="form-div-error">{this.state.errors.password}</div>
                                                }

                                            </div>
                                            <div className="form-group">
                                                <input type="password" value={this.state.confirmPassword} name='confirmPassword' onChange={this.handleChange} placeholder="Confirm Password" />
                                                {this.state.errors.confirmPassword &&
                                                    <div className="form-div-error">{this.state.errors.confirmPassword}</div>
                                                }
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    {this.state.testq ? <button type="button" className="btn btn-resent" disabled={this.state.password === '' || this.state.confirmPassword === ''} onClick={this.handleClick} data-dismiss="modal">Submit</button> : <button type="button" className="btn btn-resent" disabled={this.state.password === '' || this.state.confirmPassword === ''} onClick={this.handleClick}>Submit</button>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                }

                <div>
                    <Loader data={this.state.loading} />

                </div>

            </div>


        )
    }
}

export default Header;