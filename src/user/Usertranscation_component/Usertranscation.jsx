import React from 'react';
import '../../../public/css/style.css';

import Header from '../../common/Header';
import UserSidebar from '../../common/Usersidebar';
import axios from 'axios';
import { API_BASE_URL } from './../../constant/baseurl.js';
import _map from 'lodash/map';
import Pagination from "react-js-pagination";

class Usertranscation extends React.Component {
    constructor(props) {
        super(props);
        let abc = JSON.parse(sessionStorage.getItem('userData'));
        this.state = {
            data: abc,
            transactionHistory: [],
            sendTransactionhistory: [],
            receivedTransactionhistory: [],
            page: 1,
            perPage: 10
        }
        this.handlePageChange = this.handlePageChange.bind(this);
        this.sendPagechange = this.sendPagechange.bind(this);
        this.receivePagechange = this.receivePagechange.bind(this);
    }
    componentWillMount() {
        console.log("list componenet willl mount");

        const { history, search } = this.props;
        if (sessionStorage.getItem('userData') != null) {
            let abc = JSON.parse(sessionStorage.getItem('userData'));
            let received = [];
            let senthistroy = [];
            let totalHistroy = [];
            const tokenUrl = API_BASE_URL + "artcoin/token/api/transactionHistory";
            let sessionValu = {
                "sessionId": abc.sessionId,
                "etherWalletAddress": abc.etherWalletAddress

            }
            this.setState({ loading: true });
            axios.post(tokenUrl, sessionValu)
                .then(response => {
                    if (response.status == 200) {
                        sessionStorage.setItem('token histroy', JSON.stringify(response.data.listToken));
                        this.setState({ transactionHistory: response.data.listToken });
                      let list = response.data.listToken;
                        if (list.length > 0) {
                            list.map((utransaction, key) => {
                                if(utransaction.transactionStatus == 1){
                                if ((utransaction.toAddress == abc.etherWalletAddress) || (utransaction.fromAddress == abc.bitcoinWalletReceivingAddress)) {
                                    received.push(utransaction);
                
                                }
                            }else if(utransaction.transactionStatus == 0){
                                if ((utransaction.fromAddress == abc.etherWalletAddress) || (utransaction.fromAddress == abc.bitcoinWalletReceivingAddress)) {
                                    received.push(utransaction);
                
                                }
                            }
                            })
                        }
                        if (list.length > 0) {
                            list.map((i, key) => {
                                console.log("testt", abc.etherWalletAddress, abc.bitcoinWalletReceivingAddress);
                                if(i.transactionStatus == 1){
                                    if ((i.fromAddress == abc.etherWalletAddress) || (i.fromAddress == abc.bitcoinWalletReceivingAddress)) {
                                        senthistroy.push(i);
                    
                                    }
                                }else if(i.transactionStatus == 0){
                                    if ((i.toAddress == abc.etherWalletAddress) || (i.toAddress == abc.bitcoinWalletReceivingAddress)) {
                                        senthistroy.push(i);
                    
                                    }
                                }
                               
                
                            })
                        }
                        this.setState({sendTransactionhistory:senthistroy,receivedTransactionhistory:received});

                    }
                    else {
                        this.setState({ loading: false });
                        if (response.data.message == 'Session Expired') {
                            history.push('/login');
                        }
                        notify.show(response.data.message, "error");
                        self.setState({ errorResponse: response.data.message });
                        setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false });
                    self.setState({ errorResponse: error.message });
                    setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                });
        }


    }
    handlePageChange(page) {
        this.setState({ page })
    }
    sendPagechange(page) {
        this.setState({ page })
    }
    receivePagechange(page) {
        this.setState({ page })

    }
    render() {
        console.log("this.state in transaction list", this.state.transactionHistory);
        return (
            <div id="container">
                <Header propsdata={this.props} />
                <UserSidebar />
                <div id="main-content">
                    <div className="wrapper">
                        <div className="dashboard-title">
                            <h1>Transactions</h1>
                        </div>
                        <div className="admin-content">
                            <div className="data-menchorcoin">
                                <div className="row">
                                    <div className="col-md-12">

                                        {/* Nav tabs */}
                                        <div className="card">
                                            <ul className="nav nav-tabs" role="tablist">
                                                <li role="presentation" className="active">
                                                    <a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a>
                                                </li>
                                                <li role="presentation">
                                                    <a href="#send" aria-controls="send" role="tab" data-toggle="tab">Sent</a>
                                                </li>
                                                <li role="presentation">
                                                    <a href="#received" aria-controls="received" role="tab" data-toggle="tab">Received</a>
                                                </li>

                                            </ul>

                                            {/* Tab panes  */}
                                            <div className="tab-content">
                                                <div role="tabpanel" className="tab-pane active" id="all">
                                                    <div className="table-responsive">

                                                        {this.state.transactionHistory.length > 0
                                                            ?
                                                            <table className="table">
                                                                <thead>
                                                                    <th>S.No</th>
                                                                    <th>From Address</th>
                                                                    <th>To Address</th>
                                                                    <th>ETH Amount</th>
                                                                    <th>BTC Amount</th>
                                                                    <th>Art coins</th>
                                                                    <th>Date & Time</th>
                                                                    <th>Status</th>

                                                                </thead>
                                                                <tbody>
                                                                    {
                                                                        this.state.transactionHistory.map((utransaction, key) =>
                                                                            this.state.page * this.state.perPage > key
                                                                            && (this.state.page - 1) * this.state.perPage <= key
                                                                            &&
                                                                            <tr key={key}>
                                                                                <td>{key + 1}</td>
                                                                                <td >{utransaction.fromAddress}</td>
                                                                                <td>{utransaction.toAddress}</td>
                                                                                <td>{utransaction.etherTransferedAmount}</td>
                                                                                <td>{utransaction.bitcoinTransferedAmount}</td>
                                                                                <td>{utransaction.amount}</td>
                                                                                <td>{utransaction.createdDate}</td>
                                                                                {utransaction.transactionStatus ?
                                                                                    <td><button className="btn green">Success</button></td> :
                                                                                    <td><button className="btn label-warning">Pending</button></td>
                                                                                }

                                                                            </tr>
                                                                        )
                                                                    }
                                                                </tbody>
                                                            </table>
                                                            :
                                                            <table>
                                                                <thead>No Transactions Yet</thead>
                                                            </table>
                                                        }
                                                        <div>
                                                            {this.state.transactionHistory.length > 0 &&
                                                                <Pagination
                                                                    activePage={this.state.page}
                                                                    itemsCountPerPage={this.state.perPage}
                                                                    totalItemsCount={this.state.transactionHistory.length}
                                                                    pageRangeDisplayed={5}
                                                                    onChange={this.handlePageChange} />
                                                            }

                                                        </div>


                                                    </div>
                                                </div>
                                                <div role="tabpanel" className="tab-pane" id="send">
                                                    <div className="table-responsive">


                                                        {this.state.sendTransactionhistory.length > 0
                                                            ?
                                                            <table className="table">
                                                                <thead>
                                                                    <th>S.No</th>
                                                                    <th>From Address</th>
                                                                    <th>To Address</th>
                                                                    <th>ETH Amount</th>
                                                                    <th>BTC Amount</th>
                                                                    <th>Art coins</th>
                                                                    <th>Date & Time</th>


                                                                </thead>
                                                                <tbody>

                                                                    {
                                                                        this.state.sendTransactionhistory.map((utransaction, key) =>
                                                                            this.state.page * this.state.perPage > key
                                                                            && (this.state.page - 1) * this.state.perPage <= key
                                                                            &&
                                                                            <tr key={key}>
                                                                                <td>{key + 1}</td>
                                                                                <td >{utransaction.fromAddress}</td>
                                                                                <td>{utransaction.toAddress}</td>
                                                                                <td>{utransaction.etherTransferedAmount}</td>
                                                                                <td>{utransaction.bitcoinTransferedAmount}</td>
                                                                                <td>{utransaction.amount}</td>
                                                                                <td>{utransaction.createdDate}</td>
                                                                              

                                                                            </tr>
                                                                        )
                                                                    }
                                                                </tbody>
                                                            </table>
                                                            :
                                                            <table>
                                                                <thead>No Sent Transactions Yet</thead>
                                                            </table>

                                                        }

                                                        <div>
                                                            {this.state.sendTransactionhistory.length > 0 &&
                                                                <Pagination
                                                                    activePage={this.state.page}
                                                                    itemsCountPerPage={this.state.perPage}
                                                                    totalItemsCount={this.state.sendTransactionhistory.length}
                                                                    pageRangeDisplayed={5}
                                                                    onChange={this.sendPagechange} />
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" className="tab-pane" id="received">
                                                    <div className="table-responsive">



                                                        {this.state.receivedTransactionhistory.length > 0
                                                            ?
                                                            <table className="table">
                                                                <thead>
                                                                    <th>S.No</th>
                                                                    <th>From Address</th>
                                                                    <th>To Address</th>
                                                                    <th>ETH Amount</th>
                                                                    <th>BTC Amount</th>
                                                                    <th>Art coins</th>
                                                                    <th>Date & Time</th>


                                                                </thead>
                                                                <tbody>

                                                                    {
                                                                        this.state.receivedTransactionhistory.map((utransaction, key) =>
                                                                            this.state.page * this.state.perPage > key
                                                                            && (this.state.page - 1) * this.state.perPage <= key
                                                                            &&
                                                                            <tr key={key}>
                                                                                <td>{key + 1}</td>

                                                                                <td >{utransaction.fromAddress}</td>
                                                                                <td>{utransaction.toAddress}</td>
                                                                                <td>{utransaction.etherTransferedAmount}</td>
                                                                                <td>{utransaction.bitcoinTransferedAmount}</td>
                                                                                <td>{utransaction.amount}</td>
                                                                                <td>{utransaction.createdDate}</td>
                                                                               

                                                                            </tr>
                                                                        )
                                                                    }
                                                                </tbody>
                                                            </table>
                                                            :
                                                            <table>
                                                                <thead>No Received Transactions Yet</thead>
                                                            </table>

                                                        }
                                                        <div>
                                                            {this.state.receivedTransactionhistory.length > 0 &&
                                                                <Pagination
                                                                    activePage={this.state.page}
                                                                    itemsCountPerPage={this.state.perPage}
                                                                    totalItemsCount={this.state.receivedTransactionhistory.length}
                                                                    pageRangeDisplayed={5}
                                                                    onChange={this.receivePagechange} />
                                                            }
                                                        </div>


                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Usertranscation;