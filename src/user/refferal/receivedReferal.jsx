import React from 'react';
import Header from '../../common/Header';
import UserSidebar from '../../common/Usersidebar';
import '../../../public/css/style.css';
import { API_BASE_URL } from '../../constant/baseurl.js';
import axios from 'axios';
import Pagination from 'react-js-pagination';

class Refferal extends React.Component {
    constructor(props) {
        super(props)
        let abc = JSON.parse(sessionStorage.getItem('userData'));
        this.state = {
            data: abc,
            listReferral: [],
            page: 1,
            perPage: 10,
        }
        this.handlePageChange = this.handlePageChange.bind(this);

    }
    componentWillMount() {
        if (sessionStorage.getItem('userData') != null) {
            let abc = JSON.parse(sessionStorage.getItem('userData'));
            const props = this.props;
            const referalUrl = API_BASE_URL + "artcoin/token/api/referralReceived";
            let sessionValue = {
                "sessionId": abc.sessionId,

            }

            this.setState({ loading: true });
            axios.post(referalUrl, sessionValue)
                .then(response => {
                    if (response.status == 200) {
                        // sessionStorage.setItem('tokenhistroy', JSON.stringify(response.data.listToken));
                        this.setState({ listReferral: response.data.listReferral });
                    }
                    else {
                        if (response.data.message == 'Session Expired') {
                            props.history.push('/login');
                        }
                        this.setState({ loading: false });
                        self.setState({ errorResponse: response.data.message });
                        setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false });
                    console.log("catche", error);
                    self.setState({ errorResponse: error.message });
                    setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                });
        }

    }

    handlePageChange(page) {
        this.setState({ page })
        console.log('evt', this.state.page)
    }
    render() {
        return (

            <div id="container">
                <Header propsdata={this.props} />
                <UserSidebar />
                <div id="main-content">
                    <div className="wrapper">
                        <div className="dashboard-title">
                            <h1>Received Referral</h1>
                        </div>
                        <div class="user-board-fullwrap data-menchorcoin referral-link">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive">
                                    {this.state.listReferral.length > 0 ?
                                        <table class="table">
                                            <thead className="text-cent">
                                            <th>S.No</th>
                                                <th>Name</th>
                                                <th>Received Tokens</th>
                                                <th>Date & Time</th>
                                            </thead>
                                            <tbody className="text-cent">
                                                {
                                                    this.state.listReferral.map((utransaction, key) =>
                                                    this.state.page * this.state.perPage > key
                                                         && (this.state.page -1) * this.state.perPage <= key 
                                                         &&
                                                        <tr key={key}>
                                                        <td>{key + 1}</td>
                                                            <td >{utransaction.referentPersonName}</td>
                                                            <td>{utransaction.tokenAmount}</td>
                                                            <td>{utransaction.date}</td>

                                                        </tr>
                                                    )
                                                }

                                            </tbody>
                                        </table>
                                        :
                                        <table>
                                            <thead>No Referral Data</thead>
                                        </table>

                                    }

                                    <div class="clearfix"></div>
                                    <div>
                                        {this.state.listReferral.length > 0 &&
                                            <Pagination
                                                activePage={this.state.page}
                                                itemsCountPerPage={this.state.perPage}
                                                totalItemsCount={this.state.listReferral.length}
                                                pageRangeDisplayed={5}
                                                onChange={this.handlePageChange} />
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }

}
export default Refferal;