import React from 'react';
import '../../../public/css/style.css';

import Header from '../../common/Header';
import Sidebar from '../../common/Sidebar';

class About extends React.Component {
    render() {
        return (
            <div id="container">
                <Header />
                <Sidebar />
                <div id="main-content">
                    <div class="wrapper_section">
                        <h5 style={{marginTop: '0px',paddingTop: '10px'}}><b>Welcome</b></h5>
                        <h4 style={{color: '#f09e38'}}><b>About Us</b></h4>
                        <p style={{fontSize: '16px',fontWeight: 'bold',lineHeight: '2em'}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,.</p>
                        <p style={{fontSize: '16px',fontWeight: 'bold',lineHeight: '2em'}} class="m_top3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                        <p style={{fontSize: '16px',fontWeight: 'bold',lineHeight: '2em'}} class="m_top3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                    </div>
                </div>
            </div>
        )
    }
}

export default About;