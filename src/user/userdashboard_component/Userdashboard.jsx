import React from 'react';
import '../../../public/css/style.css';

import Header from '../../common/Header';
import UserSidebar from '../../common/Usersidebar';
import { API_BASE_URL } from '../../constant/baseurl.js';
import validator from 'validator';
import axios from 'axios';
import Loader from '../../constant/loader.js';
import Notifications, { notify } from 'react-notify-toast';

import { CopyToClipboard } from 'react-copy-to-clipboard';

class Userdashboard extends React.Component {
    constructor(props) {
        super(props);
        let abc = JSON.parse(sessionStorage.getItem('userData'));
        this.state = {
            data: abc,
            showPopup: false,
            emailId: '',
            tokenBalance: '',
            errors: {},
            reftest: true,
            coinstatus: 0,

            tokenList: [
                { value: 0, label: 'Bitcoin' },
                { value: 1, label: 'Ether' },

            ],
            loading: false,
            bitcoinToken: '',
            ethereumtokens: '',
            password: '',
            transactionHistory: [],
            payload: {},
            value: '',
            copied: false,
            bitcoinWalletAddress: abc.bitcoinWalletReceivingAddress,
            etherWalletAddress: abc.etherWalletAddress,
            etherBalance: '',
            bitcoinBalance: ''


        }
        this.handleChange = this.handleChange.bind(this);
        this.submit = this.submit.bind(this);
        this.buttonClick = this.buttonClick.bind(this);
        this.handleOnSelectChange = this.handleOnSelectChange.bind(this);
        this.togglePopup = this.togglePopup.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.forceUpdateHandler = this.forceUpdateHandler.bind(this);
        this.isEmpty = this.isEmpty.bind(this);
        this.validationForm = this.validationForm.bind(this);
        this.onCopy = this.onCopy.bind(this);
        this.bitcoinEtherBal = this.bitcoinEtherBal.bind(this);
        this.transactionHistroy = this.transactionHistroy.bind(this);
        this.balanceToken = this.balanceToken.bind(this);
        if (sessionStorage.getItem('userData') == null) {
            props.history.push('/login');
        }
        this.bitcoinEtherBal();
        this.transactionHistroy();
        this.balanceToken();

    }


    transactionHistroy() {
        const { history, search } = this.props;
        if (sessionStorage.getItem('userData') != null) {
            let abc = JSON.parse(sessionStorage.getItem('userData'));
            const tokenUrl = API_BASE_URL + "artcoin/token/api/transactionHistory";
            let sessionValu = {
                "sessionId": abc.sessionId,
                "etherWalletAddress": abc.etherWalletAddress

            }
            this.setState({ loading: true });
            axios.post(tokenUrl, sessionValu)
                .then(response => {
                    if (response.status == 200) {
                        console.log("component", response);
                        sessionStorage.setItem('tokenhistroy', JSON.stringify(response.data.listToken));
                        this.setState({ transactionHistory: response.data.listToken });
                    }
                    else {
                        this.setState({ loading: false });
                        if (response.data.message == 'Session Expired') {
                            history.push('/login');
                        }
                        notify.show(response.data.message, "error");
                        self.setState({ errorResponse: response.data.message });
                        setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false });
                    self.setState({ errorResponse: error.message });
                    setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                });
        }


    }
    onCopy() {
        this.setState({ copied: true });
        notify.show('Successfully copied!', 'success');
    };
    isEmpty(obj) {

        if (obj == null) return true;
        if (obj.length > 0) return false;
        if (obj.length === 0) return true;
        if (typeof obj !== "object") return true;
        for (var x in obj) {
            return false;
        }

        return true;

    }
    forceUpdateHandler() {
        this.props.history.push('/userdashboard');
        this.forceUpdate();
    };
    handleChange(event) {
        event.preventDefault();
        console.log("sub value", event.target.value);
        this.setState({ [event.target.name]: event.target.value });

    }
    handleOnSelectChange(event) {
        this.setState({ coinstatus: event.target.value })
    }
    buttonClick(event) {
        event.preventDefault();
        this.setState({ emailId: '' });
    }
    togglePopup() {
        console.log("bbbbbbbb", this.state.coinstatus);

        this.setState({
            showPopup: !this.state.showPopup
        });


    }
    validationForm(value) {
        const errors = {};
        if (value.emailId) {
            if (!validator.isEmail(value.emailId)) {
                errors.emailId = 'Please enter valid email address';
            }
        }
        if (value.requestTokens) {
            if (!validator.isNumeric(value.requestTokens)) {

                errors.ethereumtokens = 'Please enter valid tokens'
            }
        }
        if (value.etherWalletPassword) {
            if (value.etherWalletPassword == '' || value.etherWalletPassword == undefined) {

                errors.password = "please enter walletpassword"
            }
        }

        if (value.requestTokens) {
            if (!validator.isNumeric(value.requestTokens)) {

                errors.bitcoinToken = 'Please enter valid tokens'
            }
        }

        this.setState({ errors });
        setTimeout(() => this.setState({ errors: '' }), 2000);

        return errors
    }


    submit() {
        event.preventDefault();
        const props = this.props;
        let errors = {};
        const tokenUrl = API_BASE_URL + "artcoin/api/user/referral";
        let payload = {
            "sessionId": this.state.data.sessionId,
            'emailId': this.state.emailId
        }
        let value = this.validationForm(payload);
        this.setState({ reftest: true });

        if (!validator.isEmail(this.state.emailId)) {
            errors.emailId = 'Please enter valid email address';
            this.setState({ errors: errors });

        }

        if (errors.emailId == undefined) {
            this.setState({ loading: true });
            axios.post(tokenUrl, payload)
                .then(response => {
                    if (response.status == 200) {
                        this.setState({ test: false });
                        notify.show(response.data.message, 'success');
                        this.setState({ loading: false });
                        this.setState({ reftest: false });

                    }
                    else {
                        if (response.data.message == 'Session Expired') {
                            props.history.push('/login');
                        }
                        notify.show(response.data.message, "error");
                        this.setState({ reftest: false });


                        this.setState({ loading: false });
                        self.setState({ errorResponse: response.data.message });
                        setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                    }
                })
                .catch(function (error) {

                    this.setState({ loading: false });
                    self.setState({ errorResponse: error.message });
                    setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                });
        } else {

            this.setState({ errors });
            setTimeout(() => this.setState({ errors: '' }), 2000);
        }



    }

    bitcoinEtherBal() {

        const props = this.props;
        const tokenUrl = API_BASE_URL + "artcoin/api/user/ether/balance";
        let payload = {
            "sessionId": this.state.data.sessionId
        }

        axios.post(tokenUrl, payload)
            .then(response => {

                console.log("eth balance response>>>", response);
                if (response.status == 200) {
                    this.setState({ loading: false, etherBalance: response.data.bitcoinBalanceInfo.etherBalance });
                }
                else {
                    if (response.data.message == 'Session Expired') {
                        props.history.push('/login');
                    }
                    notify.show(response.data.message, "error");

                    this.setState({ test: false });
                    self.setState({ errorResponse: response.data.message });
                    setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                }
            })
            .catch(function (error) {
                this.setState({ loading: false });
                self.setState({ errorResponse: error.message });
                setTimeout(() => self.setState({ errorResponse: '' }), 5000);

            });

        if (this.state.data.sessionId) {
            const tokenUrl = API_BASE_URL + "artcoin/api/user/bitcoin/balance";
            let payload = {
                "sessionId": this.state.data.sessionId
            }

            axios.post(tokenUrl, payload)
                .then(response => {

                    console.log("bit balance response>>>", response);
                    if (response.status == 200) {
                        this.setState({ loading: false, bitcoinBalance: response.data.bitcoinBalanceInfo.bitcoinBalance });
                    }
                    else {
                        if (response.data.message == 'Session Expired') {
                            props.history.push('/login');
                        }
                        notify.show(response.data.message, "error");

                        this.setState({ loading: false });
                        self.setState({ errorResponse: response.data.message });
                        setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false });
                    self.setState({ errorResponse: error.message });
                    setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                });
        }


    }

    handleSubmit(event) {
        let check = this;
        event.preventDefault();
        const errors = {};
        //ether contribution
        if (this.state.coinstatus == 1) {

            this.payload = {
                "sessionId": this.state.data.sessionId,
                "selectTransactionType": this.state.coinstatus,
                "etherWalletPassword": this.state.password,
                "requestTokens": this.state.ethereumtokens
            }
            let err = this.validationForm(this.payload);
            const apiBaseUrl = API_BASE_URL + 'artcoin/token/api/contributeInCrowdsale'

            if (this.isEmpty(err)) {
                this.setState({ loading: true });
                this.setState({ errors: '' });
                axios.post(apiBaseUrl, this.payload)
                    .then(response => {
                        console.log("ether contribution successs datata>>>>", response);
                        if (response.status == 200) {
                            // this.setState({ loading: false });
                            this.setState({
                                showPopup: !this.state.showPopup,
                                coinstatus: '',
                                loading: false
                            });
                            check.props.history.push('/userdashboard');
                            notify.show(response.data.message, "success");
                            this.bitcoinEtherBal();
                            this.transactionHistroy();
                            this.balanceToken();

                        }
                        else {
                            this.setState({ loading: false });
                            if (response.data.message == 'Session Expired') {
                                check.props.history.push('/login');
                            }
                            notify.show(response.data.message, "error");

                        }
                    })
                    .catch(function (error) {
                        this.setState({ loading: false });

                    });
            }

        } else if (this.state.coinstatus == 0) {
            //bitcoin contribution bitcoinToken

            let propsdata = this;
            const apiBaseUrl = API_BASE_URL + 'artcoin/token/api/contributeInCrowdsale'
            this.payload = {
                "sessionId": this.state.data.sessionId,
                "selectTransactionType": this.state.coinstatus,
                "requestTokens": this.state.bitcoinToken,
                // "bitcoinWalletAddress": this.state.data.bitcoinWalletAddress

            }
            let err = this.validationForm(this.payload);
            if (this.isEmpty(err)) {
                this.setState({ loading: true });
                this.setState({ errors: '' });
                axios.post(apiBaseUrl, this.payload)
                    .then(response => {
                        console.log("bitcoin contribution suc res", response);
                        if (response.status == 200) {
                            // this.setState({ loading: false });
                            this.setState({
                                showPopup: !this.state.showPopup,
                                coinstatus: '',
                                loading: false
                            });
                            check.props.history.push('/userdashboard');
                            notify.show(response.data.message, "success");
                            this.bitcoinEtherBal();
                            this.transactionHistroy();
                            this.balanceToken();

                        }
                        else {
                            this.setState({ loading: false });
                            if (response.data.message == 'Session Expired') {
                                propsdata.props.history.push('/login');
                            }
                            notify.show(response.data.message, "error");

                        }
                    })
                    .catch(function (error) {
                        this.setState({ loading: false });

                    });

            }


        }

    }
    balanceToken() {
        if (sessionStorage.getItem('userData') != null) {
            const props = this.props;

            let abc = JSON.parse(sessionStorage.getItem('userData'));
            const tokenUrl = API_BASE_URL + "artcoin/token/api/token/balance";

            this.setState({ loading: true });

            let sessionValu = {
                "sessionId": abc.sessionId
            }
            axios.post(tokenUrl, sessionValu)
                .then(response => {
                    console.log("token response>>>", response);
                    if (response.status == 200) {
                        this.setState({ loading: false });
                        this.setState({ tokenBalance: response.data.TokenBalanceInfo.tokenBalance });
                        sessionStorage.setItem('tokenvalue', JSON.stringify(response.data.TokenBalanceInfo));
                        // if (res.data.loginInfo.roleId = 1) {
                        //     props.history.push('/admindashboard');

                        // } else if (res.data.loginInfo.roleId = 2) {
                        //     props.history.push('/userdashboard');
                        // }



                    }
                    else {
                        if (response.data.message == 'Session Expired') {
                            props.history.push('/login');
                        }
                        this.setState({ loading: false });
                        self.setState({ errorResponse: response.data.message });
                        setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false });
                    self.setState({ errorResponse: error.message });
                    setTimeout(() => self.setState({ errorResponse: '' }), 5000);

                });



        }

    }
    render() {
        return (
            <div id="container">
                <Header propsdata={this.props} />
                <UserSidebar />
                <div id="main-content">
                    <div className="wrapper">
                        <div className="dashboard-title">
                            <h1>User Dashboard</h1>
                        </div>
                        <div className="user-board-fullwrap">

                            {/* <div class="col-md-12 col-xs-12 col-sm-12">
                                <div class="referal-link">
                                    <a href="" data-toggle="modal" data-target="#referral">Referral</a>
                                </div>
                            </div> */}
                            <div class="col-md-12 col-xs-12 col-sm-12">
                                <div class="referal-link">
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#referral">Referral</a>
                                </div>
                            </div>
                            <div className="col-md-12 col-xs-12 col-sm-12">
                                <div className="menchor-wallet-id">
                                    <p>Bitcoin Address :
                               {this.state.data != null && <span>{this.state.data.bitcoinWalletReceivingAddress}</span>}
                                    </p>
                                    <div className="copyclip"><CopyToClipboard onCopy={this.onCopy} text={this.state.bitcoinWalletAddress}>
                                        <span>Copy</span>
                                    </CopyToClipboard></div>
                                </div>
                                <div className="menchor-wallet-id">
                                    <p>Ether Address :
                                {this.state.data != null && <span>{this.state.data.etherWalletAddress}</span>}
                                    </p>
                                    <div className="copyclip"><CopyToClipboard onCopy={this.onCopy} text={this.state.etherWalletAddress}>
                                        <span>Copy</span>
                                    </CopyToClipboard></div>
                                </div>
                            </div>
                            <div className="balance-list-box">
                                <div className="col-md-4 col-xs-12 col-sm-4">
                                    <div className="list-box-3">
                                        <h2>Bitcoin balance</h2>
                                        <img src="public/image/user-bit-coin.png" />
                                        {this.state.bitcoinBalance != null && <h4>{this.state.bitcoinBalance}</h4>}
                                    </div>
                                </div>
                                <div className="col-md-4 col-xs-12 col-sm-4">
                                    <div className="list-box-3">
                                        <h2>Ether Balance</h2>
                                        <img src="public/image/etherum.png" />
                                        {this.state.etherBalance != null && <h4>{this.state.etherBalance}</h4>}
                                    </div>
                                </div>
                                <div className="col-md-4 col-xs-12 col-sm-4">
                                    <div className="list-box-3">
                                        <h2>Token Balance</h2>
                                        <img src="public/image/token-bal.png" />
                                        {this.state.data != null && <h4>{this.state.tokenBalance}</h4>}
                                    </div>
                                </div>
                            </div>
                            <div className="transaction-wrap">
                                <div className="col-md-12 col-sm-12 col-xs-12">
                                    <div className="transaction-title">
                                        <h1>transaction</h1>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12 col-sm-6">
                                            <div className="purchase-coin">
                                                <div className="purchse-title">
                                                    <h1>Purchase Token</h1>
                                                </div>
                                                <div className="purchase-body">
                                                    <div className="purchase-div">

                                                        {/* <input type="text" /> */}
                                                        {/* <select style={{ padding: 10 }} name="tokenAddress" value={this.state.coinstatus} onChange={this.handleOnSelectChange}>
                                                            <option value="">Select Currency</option>
                                                            {this.state.tokenList.map((location, index) => {
                                                                return <option key={index} value={location.value} >{location.label}</option>;
                                                            })}
                                                        </select> */}
                                                        <h1>Select Currency</h1>
                                                        {/* <input type="radio" value='0'
                                                            checked={this.state.coinstatus == 0}
                                                            onChange={this.handleOnSelectChange} />
                                                        BTC
                                                        <input type="radio" value='1'
                                                            checked={this.state.coinstatus == 1}
                                                            onChange={this.handleOnSelectChange} />
                                                        ETH */}
                                                        <form action="#">
                                                            <p>
                                                                <input type="radio" id="test1" name="radio-group" value='0'
                                                                    checked={this.state.coinstatus == 0}
                                                                    onChange={this.handleOnSelectChange} />
                                                                <label for="test1">BTC</label>
                                                            </p>
                                                            <p>
                                                                <input type="radio" id="test2" name="radio-group" value='1'
                                                                    checked={this.state.coinstatus == 1}
                                                                    onChange={this.handleOnSelectChange} />
                                                                <label for="test2">ETH</label>
                                                            </p>

                                                        </form>
                                                    </div>
                                                    <div className="purchase-bttn">
                                                        <button type="button" onClick={this.togglePopup.bind(this)} className="btn btn-purchase-coin">Purchase Token</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6 col-xs-12 col-sm-6">
                                            <div className="purchase-coin">
                                                <div className="purchse-title dark-b">
                                                    <h1>Recent Transaction</h1>
                                                </div>
                                                <div className="purchase-body user-body">
                                                    {this.state.transactionHistory.length > 0 ?

                                                        < ul >
                                                            {
                                                                this.state.transactionHistory.slice(0, 3).map(i => {
                                                                    return <li >
                                                                        <div className="user-profile">
                                                                            <div className="user-avatar">
                                                                                <img src="public/image/user-1.png" />
                                                                            </div>
                                                                            <div className="user-details">
                                                                                {i.transactionStatus == 0 ?
                                                                                    <p>Pending {i.amount} Artcoins from {i.fromAddress}</p>
                                                                                    :
                                                                                    <p>Success {i.amount} Artcoins from {i.fromAddress}</p>

                                                                                }
                                                                                <span>{i.createdDate}</span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                })
                                                            }
                                                        </ul> :
                                                        <ul>
                                                            <li>No Recent Transactions Yet</li>
                                                        </ul>

                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* <Loader data={this.state.loading} /> */}

                {this.state.reftest &&
                    <div className="reset-password-wrap">
                        <div className="modal fade" id="referral" role="dialog">
                            <div className="modal-dialog">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <button type="button" className="close" onClick={this.buttonClick} data-dismiss="modal">&times;</button>
                                        <h4 className="modal-title referal-title">Referral</h4>
                                    </div>
                                    <div className="modal-body">
                                        <Notifications />

                                        <div className="reset-password">
                                            <form action="" method="post">
                                                <div className="form-group">
                                                    <input type="text" value={this.state.emailId} name='emailId' onChange={this.handleChange} placeholder="Email" />
                                                    {this.state.errors.emailId &&
                                                        <div className="form__field" style={{ color: 'red' }}>{this.state.errors.emailId}</div>
                                                    }
                                                </div>
                                            </form>


                                        </div>

                                    </div>


                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-resent" disabled={this.state.emailId === ''} onClick={this.submit}>Submit</button>
                                    </div>
                                    <Loader data={this.state.loading} />
                                </div>
                            </div>
                        </div>

                    </div>
                }
                {/* {this.state.reftest &&
                <div className="reset-password-wrap">
                    <div className="modal fade" id="referral" role="dialog">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title referal-title">Referral</h4>
                                </div>
                                <div className="modal-body">
                                    <div className="reset-password">
                                        <form action="" method="post">
                                            <div className="form-group">
                                                <input type="text" value="" placeholder="Email"/>
                                </div>
                            </form>
                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-resent" data-dismiss="modal">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
} */}
                {this.state.showPopup ?
                    <Popup
                        text='Close Me' data={this.state}
                        closePopup={this.togglePopup.bind(this)}
                        handle={this.handleChange.bind(this)}
                        submit={this.handleSubmit.bind(this)}
                    />
                    : null
                }

            </div>


        )
    }
}

class Popup extends React.Component {
    render() {
        return (
            <div>
                <div>
                    {this.props.data.coinstatus == 0 &&
                        <div className='popup'>
                            <div className='popup_inner'>
                                <h1>Bitcoin</h1>
                                <div className="popup_inner_body">
                                    <div className="form-group">
                                        <label>Tokens:</label>
                                        <input type='text' name='bitcoinToken' onChange={this.props.handle} />
                                        {
                                            this.props.data.errors.bitcoinToken &&
                                            <div className="form__field" style={{ color: 'red' }}>{this.props.data.errors.bitcoinToken}</div>
                                        }
                                    </div>

                                    <div className="bttn">
                                        <button type="button" onClick={this.props.submit} disabled={this.props.data.bitcoinToken === ''} >submit</button>
                                        <button className="btn-red" type="button" onClick={this.props.closePopup} >Cancel</button>
                                    </div>
                                    <Loader data={this.props.data.loading} />
                                </div>
                            </div>
                        </div>

                    }
                </div>
                <Notifications />
                <div>
                    {this.props.data.coinstatus == 1 &&
                        <div className='popup'>
                            <div className='popup_inner height'>
                                <h1>Etherium</h1>
                                <div className="popup_inner_body">
                                    <div className="form-group"><label>Tokens:</label>
                                        <input type='text' name='ethereumtokens' onChange={this.props.handle} />
                                        {
                                            this.props.data.errors.ethereumtokens &&
                                            <div className="form__field" style={{ color: 'red' }}>{this.props.data.errors.ethereumtokens}</div>
                                        }
                                    </div>

                                    <div className="form-group">
                                        <label>Wallet Password:</label>
                                        <input type='password' name='password' onChange={this.props.handle} />
                                        {
                                            this.props.data.errors.password &&
                                            <div className="form__field" style={{ color: 'red' }}>{this.props.data.errors.password}</div>
                                        }
                                    </div>
                                    <div className="bttn">
                                        <button onClick={this.props.submit} disabled={this.props.data.ethereumtokens === '' || this.props.data.password === ''}>submit</button>
                                        <button className="btn-red" onClick={this.props.closePopup}>Cancel</button>
                                    </div>
                                </div>
                                <Loader data={this.props.data.loading} />

                            </div>
                        </div>

                    }

                </div>

            </div>
        );
    }
}
export default Userdashboard;