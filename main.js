import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import Login from './src/admin/login_component/Login.jsx';
import Register from './src/admin/Register_component/Register.jsx';
import Forgotpassword from './src/admin/Forgotpassword_component/Forgotpassword.jsx';
import Admindashboard from './src/admin/admindashboard_component/Admindashboard.jsx';
import AdminTranscation from './src/admin/Transcation_component/AdminTranscation.jsx';
import UserDashboard from './src/user/userdashboard_component/Userdashboard.jsx';
import UserTranscation from './src/user/Usertranscation_component/Usertranscation';
import Support from './src/user/support/support.jsx';
import Refferal from './src/user/refferal/receivedReferal.jsx';
import confirmation from './src/admin/Register_component/confirmationemail.jsx';
import resetPassword from './src/admin/Forgotpassword_component/resetpassword.jsx';
import userlist from './src/admin/userlist/userlist.jsx';



// import About from './src/user/about_component/About.jsx';



ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Redirect exact path="/" to="/login" />
            <Route path='/login' component={Login} />
            <Route path='/register' component={Register} />
            <Route path='/forgotpassword' component={Forgotpassword} />
            <Route path='/admindashboard' component={Admindashboard} />
            <Route path='/admintranscation' component={AdminTranscation} />  
            <Route path='/userdashboard' component={UserDashboard} /> 
            <Route path='/usertranscation' component={UserTranscation} /> 
            <Route path='/Support' component={Support} />
            <Route path='/Refferal' component={Refferal} />
            <Route path='/confirmation' component={confirmation} />
            <Route path='/resetPassword' component={resetPassword} />
            <Route path='/userlist' component={userlist} />

            {/* 
                  
            <Route path='/userdashboard' component={UserDashboard} />
            <Route path='/about' component={About} />  */}
        </Switch>
    </BrowserRouter>
    , document.getElementById('app'));